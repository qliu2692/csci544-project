#  python2.7

#  Author: Mengchun Wu
#  Date: 4/20/2015
#
#  This code is to get model of Word2Vec.
#  This code is not written all by myself. 
#  I revised open-source code from Kaggle:
#  https://www.kaggle.com/c/word2vec-nlp-tutorial
# *************************************** #

import pandas as pd
import os
from nltk.corpus import stopwords
import nltk.data
import logging
import numpy as np  # Make sure that numpy is imported
from gensim.models import Word2Vec

import sys
import json
from sklearn.preprocessing import Imputer
from collections import defaultdict
from sklearn import cluster
from sklearn.metrics import f1_score

def review_to_wordlist( review, remove_stopwords=False ):
    words=review.split()       
    return(words)

def review_to_sentences( review, tokenizer, remove_stopwords=False ):
    raw_sentences = tokenizer.tokenize(review.decode('utf8').strip())
    sentences = []
    for raw_sentence in raw_sentences:
        # If a sentence is empty, skip it
        if len(raw_sentence) > 0:
            sentences.append(review_to_wordlist( raw_sentence, \
                  remove_stopwords ))
    
    # Return the list of sentences (each sentence is a list of words,
    # so this returns a list of lists
    return sentences


if __name__ == '__main__':

    # Read data from files
    input1=sys.argv[1]   # train data
    input2=sys.argv[2]   # dictionary of tags
    
    train=json.load(open(input1))
    tag_dic=json.load(open(input2))
    
    traindata = []
    testdata = []

    Y1=[]
    Y2=[]

    for i in train:
        buf=[]
        traindata.append(train[i][0]+train[i][1])
        for j in train[i][3].split():
            if j in tag_dic:
               buf.append(tag_dic[j])
        Y1.append(buf)


    # Load the punkt tokenizer
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    sentences = []  # Initialize an empty list of sentences

    for review in traindata:
        sentences += review_to_sentences(review.encode('utf-8'), tokenizer)

    # ****** Set parameters and train the word2vec model
    #
    # Import the built-in logging module and configure it so that Word2Vec
    # creates nice output messages
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s',\
        level=logging.INFO)

    # Set values for various parameters
    num_features = 300    # Word vector dimensionality
    min_word_count = 40   # Minimum word count
    num_workers = 4       # Number of threads to run in parallel
    context = 10          # Context window size
    downsampling = 1e-3   # Downsample setting for frequent words

    # Initialize and train the model (this will take some time)
    model = Word2Vec(sentences, workers=num_workers, \
                size=num_features, min_count = min_word_count, \
                window = context, sample = downsampling, seed=1)

    model.init_sims(replace=True)

    # Create a model named "300features_40minwords_10context"
    model_name = "300features_40minwords_10context"
    model.save(model_name)
