#  python2.7

#  Author: Mengchun Wu
#  Date: 4/20/2015
#
#  This code is to use KMeans to get word vector
#
# *************************************** #


# Load a pre-trained model
from gensim.models import Word2Vec
from sklearn.cluster import KMeans
import json
import sys
import pandas as pd
import re
from nltk.corpus import stopwords
import numpy as np
import os
from collections import defaultdict

def review_to_wordlist( review, remove_stopwords=False ):
    words=review.split()       
    return(words)

def review_to_sentences( review, tokenizer, remove_stopwords=False ):
    raw_sentences = tokenizer.tokenize(review.decode('utf8').strip())
    sentences = []
    for raw_sentence in raw_sentences:
        # If a sentence is empty, skip it
        if len(raw_sentence) > 0:
            sentences.append(review_to_wordlist( raw_sentence, \
                  remove_stopwords ))
    
    # Return the list of sentences (each sentence is a list of words,
    # so this returns a list of lists
    return sentences

if __name__ == '__main__':

    model = Word2Vec.load("300features_40minwords_10context")

    input1=sys.argv[1]   # train data
    input2=sys.argv[2]   # test data
    tagdic=sys.argv[3]   # dictionary for tag 100

    train_file=json.load(open(input1))
    test_file=json.load(open(input2))

    tag_dic=json.load(open(tagdic))

    # Initialize an empty list to hold the clean reviews
    train = []
    test = []

    Y1=[]
    Y2=[]

    # Create word list for train and test data


    for i in train_file:
        buf=[]
        train.append(" ".join(review_to_wordlist(train_file[i][0]+train_file[i][1], True)))
        for j in train_file[i][3].split():
            if j in tag_dic:
               buf.append(tag_dic[j])
        Y1.append(buf)



    for i in test_file:
        buf=[]
        test.append(" ".join(review_to_wordlist(test_file[i][0]+test_file[i][1], True)))
        for j in test_file[i][3].split():
            if j in tag_dic:
                buf.append(tag_dic[j])
        Y2.append(buf)

    # ****** Run k-means on the word vectors and print a few clusters

    # Set "k" (num_clusters) to be 1/40th of the vocabulary size, or an
    # average of 40 words per cluster
    word_vectors = model.syn0
    num_clusters = word_vectors.shape[0] / 40

    # Initalize a k-means object and use it to extract centroids
    kmeans_clustering = KMeans( n_clusters = num_clusters)
    idx = kmeans_clustering.fit_predict( word_vectors )

    # Create a Word / Index dictionary, mapping each vocabulary word to
    # a cluster number
    word_centroid_map = dict(zip( model.index2word, idx ))

    dict_cluster=defaultdict()
    for cluster in xrange(0,2000):
        # Find all of the words for that cluster number, and print them out
        words = []
        for i in xrange(0,len(word_centroid_map.values())):
            if( word_centroid_map.values()[i] == cluster ):
                words.append(word_centroid_map.keys()[i])
        dict_cluster[cluster]=words

    json.dump(dict_cluster, open('dict_cluster.txt','w'))



