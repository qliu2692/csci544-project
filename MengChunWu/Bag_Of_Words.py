#  python 2.7

#  Author: Mengchun Wu
#  Date: 4/20/2015
#
#  This code is to implement bag of words
#
# *************************************** #

import os
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
import numpy as np
import json
import sys
import time

from collections import defaultdict
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.metrics import f1_score

def review_to_wordlist(review):

    words=review.split()

    return(words)

if __name__ == '__main__':
    input1=sys.argv[1]   # train data
    input2=sys.argv[2]   # test data
    tagdic=sys.argv[3]   # dictionary for tag 100

    train=json.load(open(input1))
    test=json.load(open(input2))
    
    tag_dic=json.load(open(tagdic))


    # Initialize an empty list to hold the clean dataset
    traindata = []
    testdata = []
     
    Y1=[]
    Y2=[]
    
    # Loop over each instance and create wordlist for train and test
    
    for i in train:
        buf=[]
        traindata.append(" ".join(review_to_wordlist(train[i][0]+train[i][1])))
        for j in train[i][3].split():
            if j in tag_dic:
               buf.append(tag_dic[j])
        Y1.append(buf)



    for i in test:
        buf=[]
        testdata.append(" ".join(review_to_wordlist(test[i][0]+test[i][1])))
        for j in test[i][3].split():
            if j in tag_dic:
               buf.append(tag_dic[j])
        Y2.append(buf)
    # Create a bag of words from the training set

    vectorizer = CountVectorizer(min_df=0.001)


    X_all=traindata+testdata
    lentrain=len(traindata)

    X = vectorizer.fit_transform(X_all)

    X_train = X[:lentrain]
    X_test = X[lentrain:]

    X1 = X_train.toarray()
    X2 = X_test.toarray()

    clf=LinearSVC(random_state=0)

    classif = OneVsRestClassifier(clf).fit(X1, Y1)
    class_set=classif.classes_
    scores=classif.decision_function(X2)
    Y3=[]
    
    # Start predict tags
    # if score >0, print out that class
    # if there's no score >0, print out the class with largest score
        
    if len(scores.shape) == 1:
        indices = (scores > 0).astype(np.int)
    else:
        for score in scores:
            buf=[]
            for i in range(9):
                if score[i]>0:
                   buf.append(class_set[i])
            if not buf:
                   indices = np.argmax(score)
                   
                   buf.append(class_set[indices])
            Y3.append(buf)

    # print out predicted tags

    for i in Y3:
        print(i)

    # print out f1-score
    print(f1_score(Y3,Y2))
