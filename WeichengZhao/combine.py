#import json
from sklearn import preprocessing
from sklearn import metrics
import simplejson
import sys

def clean(line):
    cleaned = line.replace('(', '[')
    cleaned = cleaned.replace(')', ']')
    cleaned = cleaned.replace(',]', ']')
    return cleaned

def f_score(result, predict):
    correct = 0
    classify = 0
    belongs = 0
    
    # correct clssify / classified
    for i in range(len(result)):
        for predict in result[i]:
            classify += 1
            if predict in standard[i]:
                correct += 1

    # belongs
    for i in range(len(standard)):
        for elem in standard[i]:
            belongs += 1

    #import pdb
    #pdb.set_trace()

    precision = correct * 100.0 / classify
    recall = correct * 100.0 / belongs
    fscore = 2 * precision * recall / ( precision + recall )

    print "Presicion: ", precision
    print "Recall: ", recall
    print "F-score: ", fscore
    return [precision, recall, fscore]

# body_tfidf.txt  bow.txt     code_tfidf.txt  combine.py  tag_test.txt    title.txt
f_bt = open('body_tfidf.txt', 'r')
f_bo = open('bow.txt', 'r')
f_ct = open('code_tfidf.txt', 'r')
f_tt = open('tag_test.txt', 'r')
f_ti = open('title_tfidf.txt', 'r')
f_tb = open('title_bow.txt', 'r')
f_tl = open('title.txt', 'r')


files = [f_bt, f_bo, f_ct, f_ti, f_tb, f_tl]
name = ["body_tfidf", "bow_body", "bow_code", "code_tfidf", "title_tfidf", "title_bow", "title_key_word"] 
prediction = []
standard = []
result = [] # [result, 'name']

num = 0
for line in f_tt:
    num += 1
    if num == 3:
        standard = simplejson.loads(clean(line))

ids = 0
for f in files:
    ids += 1
    num = 0
    for line in f:
        num += 1
        if num == 3:
            prediction.append( simplejson.loads(clean(line)) )
        if ids == 2 and num == 10:
            prediction.append( simplejson.loads(clean(line)) )

        
print "one"
print "============================================================"
for i in range(len(prediction)):
        combine = []
        for index in range(len(prediction[i])):
            combine.append(list(prediction[i][index]))
        empty = 0
        for elem in combine:
            if elem == []:
                empty += 1
        print "empty:", empty
        n = name[i]
        print n

        f_l = f_score(combine, standard)
        result.append([f_l[2], f_l, n])

        #mlb = preprocessing.MultiLabelBinarizer()
        #std = mlb.fit_transform(standard)
print "============================================================"

print "COMBINATION OF TWO"
print "============================================================"
for i in range(len(prediction)):
    for j in range(len(prediction))[i+1:]:
        combine = []
        for index in range(len(prediction[i])):
            combine.append(list(set(prediction[i][index] + prediction[j][index])))
        empty = 0
        for elem in combine:
            if elem == []:
                empty += 1
        print "empty:", empty
        n = name[i], "+", name[j]
        print n

        f_l = f_score(combine, standard)
        result.append([f_l[2], f_l, n])

        #mlb = preprocessing.MultiLabelBinarizer()
        #std = mlb.fit_transform(standard)
        #rst = mlb.fit_transform(combine)


        #print "\nsklearn f1-score (weighted):", metrics.f1_score(std, rst, average='weighted') * 100
        #print "sklearn f1-score    (micro):", metrics.f1_score(std, rst, average='micro') * 100
        #print "sklearn f1-score    (macro):", metrics.f1_score(std, rst, average='macro') * 100
        print "============================================================"

print "COMBINATION OF THREE"
print "============================================================"
for i in range(len(prediction)):
    for j in range(len(prediction))[i+1:]:
        for k in range(len(prediction))[j+1:]:
            combine = []
            for index in range(len(prediction[i])):
                combine.append(list(set(prediction[i][index] + prediction[j][index] + prediction[k][index])))
            empty = 0
            for elem in combine:
                if elem == []:
                    empty += 1
            print "empty:", empty
            n = name[i], "+", name[j], "+", name[k]
            print n

            f_l = f_score(combine, standard)
            result.append([f_l[2], f_l, n])

            #mlb = preprocessing.MultiLabelBinarizer()
            #std = mlb.fit_transform(standard)
            #rst = mlb.fit_transform(combine)

            #print "\nsklearn f1-score (weighted):", metrics.f1_score(std, rst, average='weighted') * 100
            #print "sklearn f1-score    (micro):", metrics.f1_score(std, rst, average='micro') * 100
            #print "sklearn f1-score    (macro):", metrics.f1_score(std, rst, average='macro') * 100
            print "============================================================"

print "COMBINATION OF FOUR"
print "============================================================"
for i in range(len(prediction)):
    for j in range(len(prediction))[i+1:]:
        for k in range(len(prediction))[j+1:]:
            for l in range(len(prediction))[k+1:]:
                combine = []
                for index in range(len(prediction[i])):
                    combine.append(list(set(prediction[i][index] + prediction[j][index] + prediction[k][index] + prediction[l][index])))
                empty = 0
                for elem in combine:
                    if elem == []:
                        empty += 1
                print "empty:", empty
                n = name[i], "+", name[j], "+", name[k], "+", name[l]
                print n

                f_l = f_score(combine, standard)
                result.append([f_l[2], f_l, n])

                #mlb = preprocessing.MultiLabelBinarizer()
                #std = mlb.fit_transform(standard)
                #rst = mlb.fit_transform(combine)

                #print "\nsklearn f1-score (weighted):", metrics.f1_score(std, rst, average='weighted') * 100
                #print "sklearn f1-score    (micro):", metrics.f1_score(std, rst, average='micro') * 100
                #print "sklearn f1-score    (macro):", metrics.f1_score(std, rst, average='macro') * 100
                print "============================================================"

print "COMBINATION OF FIVE"
print "============================================================"
for i in range(len(prediction)):
    for j in range(len(prediction))[i+1:]:
        for k in range(len(prediction))[j+1:]:
            for l in range(len(prediction))[k+1:]:
                for m in range(len(prediction))[l+1:]:
                    combine = []
                    for index in range(len(prediction[i])):
                        combine.append(list(set(prediction[i][index] + prediction[j][index] + prediction[k][index] + prediction[l][index] + prediction[m][index])))
                    empty = 0
                    for elem in combine:
                        if elem == []:
                            empty += 1
                    print "empty:", empty
                    n = name[i], "+", name[j], "+", name[k], "+", name[l], "+", name[m]
                    print n

                    f_l = f_score(combine, standard)
                    result.append([f_l[2], f_l, n])

                    #mlb = preprocessing.MultiLabelBinarizer()
                    #std = mlb.fit_transform(standard)
                    #rst = mlb.fit_transform(combine)

                    #print "\nsklearn f1-score (weighted):", metrics.f1_score(std, rst, average='weighted') * 100
                    #print "sklearn f1-score    (micro):", metrics.f1_score(std, rst, average='micro') * 100
                    #print "sklearn f1-score    (macro):", metrics.f1_score(std, rst, average='macro') * 100
                    print "============================================================"

print "COMBINATION OF SIX"
print "============================================================"
combine = []
for index in range(len(prediction[0])):
    combine.append(list(set(prediction[0][index] + prediction[1][index] + prediction[2][index] + prediction[3][index] + prediction[4][index] + prediction[5][index])))
empty = 0
for elem in combine:
    if elem == []:
        empty += 1
print "empty:", empty
n = name[0], "+", name[1], "+", name[2], "+", name[3], "+", name[4], "+", name[5]
f_l = f_score(combine, standard)
result.append([f_l[2], f_l, n])
#mlb = preprocessing.MultiLabelBinarizer()
#std = mlb.fit_transform(standard)
#rst = mlb.fit_transform(combine)

#print "\nsklearn f1-score (weighted):", metrics.f1_score(std, rst, average='weighted') * 100
#print "sklearn f1-score    (micro):", metrics.f1_score(std, rst, average='micro') * 100
#print "sklearn f1-score    (macro):", metrics.f1_score(std, rst, average='macro') * 100
print "============================================================"

result.sort()
for elem in result:
    print "F1-score: ", elem[1][2], "precision:", elem[1][0], "recall:", elem[1][1], ''.join(elem[2])
